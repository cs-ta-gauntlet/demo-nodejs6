import {hash_md5, hash_sha256, hash_sha512} from "../handler"

test("test hash_md5", () => {
    let test_payload = {'data' : {'value': "This is a test string"}};
    expect(hash_md5(test_payload)).toEqual("c639efc1e98762233743a75e7798dd9c");

    test_payload = {'data' : {'value': "This is another string to be tested"}};
    expect(hash_md5(test_payload)).toEqual("0639561ca26e3c715bc18c98b880cf50");
});

test("test hash_sha256", () => {
    let test_payload = {'data' : {'value': "This is a test string"}};
    expect(hash_sha256(test_payload)).toEqual("717ac506950da0ccb6404cdd5e7591f72018a20cbca27c8a423e9c9e5626ac61");

    test_payload = {'data' : {'value': "This is another string to be tested"}};
    expect(hash_sha256(test_payload)).toEqual("0ecad0bea55f5fd1626b206d6378cded3465bf1df67d6fc7ceda4e5e0116cf8c");
});

test("test hash_sha512", () => {
    let test_payload = {'data' : {'value': "This is a test string"}};
    expect(hash_sha512(test_payload)).toEqual("b8ee69b29956b0b56e26d0a25c6a80713c858cf2902a12962aad08d682345646b2d5f193bbe03997543a9285e5932f34baf2c85c89459f25ba1cf43c4410793c");

    test_payload = {'data' : {'value': "This is another string to be tested"}};
    expect(hash_sha512(test_payload)).toEqual("6e8db62927ce18347f83f25cfc9776ea1325d207350323655a50b3ce3ab99c6e2b0218e6893c20e4c1fdaf85d94b2915603314db88e3dc90c379442b61a14a5b");
});