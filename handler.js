'use strict';

import crypto from "crypto";

module.exports = {
    hash_md5(event, context){
        return hash(event.data.value, "md5");
    },
    hash_sha256(event, context){
        return hash(event.data.value, "sha256");
    },
    hash_sha512(event, context){
        return hash(event.data.value, "sha512");
    },
};

function hash(to_be_hashed, hash_algorithm){
    return crypto.createHash(hash_algorithm).update(to_be_hashed).digest('hex');
}